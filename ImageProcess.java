import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageProcess {
	/**
	 * Resize the image into 8*8 image
	 * @param inputImagePath the path of input image
	 * @return return the resized image
	 * @throws IOException
	 */
	public BufferedImage image;
	public BufferedImage gray;
	public Color averageColor;
	public Color averageGray;
	public String fingerPrint;
	public ImageProcess(String inputImagePath) throws IOException{
		image = resize(inputImagePath);
		averageColor = average(image);
		gray = gray(image);
		averageGray = average(gray);
		fingerPrint = fingerPrint(gray);
	}
	
	public String compareTo(ImageProcess other){
		double count = 0;
		String out = "";
		for(int i = 0; i < fingerPrint.length(); i++){
			if(fingerPrint.charAt(i) == other.fingerPrint.charAt(i)){
				count++;
			}
		}
		double percent = count/fingerPrint.length();
		percent = Math.round(percent * 10000.0) / 100.0;
		out = "Structure similarity is: " + percent + "%. "; 
		out += colorCompareTo(other);
		return out;
	}
	
	private String colorCompareTo(ImageProcess other){
		double red = Math.abs(averageColor.getRed() - other.averageColor.getRed());
		double blue = Math.abs(averageColor.getBlue() - other.averageColor.getBlue());
		double green = Math.abs(averageColor.getGreen() - other.averageColor.getGreen());
		
		double result = 1 - (red/255 + blue/255 + green/255)/3;
		result = Math.round(result * 10000.0) / 100.0;
		return "Color similarity is: "+result + "%.";
		
	}
	
	private String fingerPrint(BufferedImage image){
		String out = "";
		for (int y = 0; y < image.getHeight(); y++) {
		    for (int x = 0; x < image.getWidth(); x++) {
		    	if(similarTo(new Color(image.getRGB(x, y)), averageGray)){
		    		out += "0";
		    	}
		    	else{
		    		out+="1";
		    	}
		    }
		}
		return out;
	}
	
	private boolean similarTo(Color c1, Color c2){
	    double distance = (c1.getRed() - c2.getRed()) + (c1.getGreen() - c2.getGreen()) + (c1.getBlue() - c2.getBlue());
	    if(distance < 0){
	        return true;
	    }else{
	        return false;
	    }
	}
	
	private static Color average(BufferedImage image){
		long sumr = 0, sumg = 0, sumb = 0;
		for (int y = 0; y < image.getHeight(); y++) {
		    for (int x = 0; x < image.getWidth(); x++) {
	            Color pixel = new Color(image.getRGB(x, y));
	            sumr += pixel.getRed();
	            sumg += pixel.getGreen();
	            sumb += pixel.getBlue();	 
		    }
		}
		float pixels = image.getHeight() * image.getWidth();
		float r = sumr/pixels;
		float g = sumg/pixels;
		float b = sumb/pixels;
		return new Color(r/255, g/255, b/255);
	}
	
	private static BufferedImage gray(BufferedImage image){
		BufferedImage grayImg = new BufferedImage(8, 8, image.getType());
		for (int y = 0; y < image.getHeight(); y++) {
		    for (int x = 0; x < image.getWidth(); x++) {
		    	int p = image.getRGB(x,y);
		        int a = (p>>24)&0xff;
		        int r = (p>>16)&0xff;
		        int g = (p>>8)&0xff;
		        int b = p&0xff;
		        int avg = (r+g+b)/3;
		        p = (a<<24) | (avg<<16) | (avg<<8) | avg;
		        grayImg.setRGB(x, y, p);
		    }
		}
		return grayImg;
	}
	
	private static BufferedImage resize(String inputImagePath) throws IOException {
		File inputFile = new File(inputImagePath);
		BufferedImage inputImage = ImageIO.read(inputFile);
		BufferedImage result = new BufferedImage(8, 8, BufferedImage.TYPE_3BYTE_BGR);
		Graphics g = result.getGraphics();
		g.drawImage(inputImage.getScaledInstance(8, 8, Image.SCALE_SMOOTH), 0, 0, null);
		g.dispose();
		return result;
	}

}
